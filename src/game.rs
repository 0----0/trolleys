mod input;

use wasm_bindgen::prelude::*;
use input::InputHandler;
use crate::renderer::{Renderer, Sprite};


#[wasm_bindgen]
pub struct Game {
    renderer: Renderer,
    input_handler: InputHandler,
    x: f32,
    y: f32,
}

#[wasm_bindgen]
impl Game {
    pub fn new() -> Result<Game, JsValue> {
        Ok(Game{
            renderer: Renderer::new()?,
            input_handler: InputHandler::new(),
            x: 0.0,
            y: 0.0,
        })
    }
    pub fn init(&mut self) -> Result<(), JsValue> {
        self.renderer.init()?;
        self.input_handler.add_key(87);
        self.input_handler.add_key(65);
        self.input_handler.add_key(83);
        self.input_handler.add_key(68);
        Ok(())
    }

    pub fn update(&mut self) {
        let speed = 0.25;
        if self.input_handler.get_key(87) {
            self.y += speed;
        }
        if self.input_handler.get_key(65) {
            self.x -= speed;
        }
        if self.input_handler.get_key(83) {
            self.y -= speed;
        }
        if self.input_handler.get_key(68) {
            self.x += speed;
        }
    }

    pub fn key_down(&mut self, keycode: u32) {
        self.input_handler.set_key(keycode, true);
    }

    pub fn key_up(&mut self, keycode: u32) {
        self.input_handler.set_key(keycode, false);
    }

    pub fn draw(&self) {
        let renderer = &self.renderer;
        let scene = [
            Sprite { x: -100.0 - self.x, y: -100.0 - self.y, w: 256.0, h: 256.0, img: "./assets/bg_test.png"},
            Sprite { x: -16.0, y: -32.0, w: 32.0, h: 64.0, img: "./assets/person_test.png"},
        ];
        renderer.draw(&scene);
    }
}
