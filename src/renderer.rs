
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{HtmlImageElement, WebGlProgram, WebGlRenderingContext as GL, WebGlTexture};
use std::rc::{Rc, Weak};
use std::cell::RefCell;
use std::collections::HashMap;

mod shader;

use shader::{compile_shader, link_program};


#[wasm_bindgen]
extern {
    fn alert(s: &str);
    fn loadImage(src: &str) -> js_sys::Promise;
}


async fn load_image(src: &str) -> Result<HtmlImageElement, JsValue> {
    wasm_bindgen_futures::JsFuture::from(loadImage(src)).await.and_then(|j| { j.dyn_into::<HtmlImageElement>() })
}



struct Buffer {
    len: i32,
    buffer: web_sys::WebGlBuffer,
}

impl Buffer {
    fn new(context: &GL, vertices: &[f32]) -> Result<Self, JsValue> {
        let buffer = context.create_buffer().ok_or("failed to create buffer")?;
        context.bind_buffer(GL::ARRAY_BUFFER, Some(&buffer));

        // Note that `Float32Array::view` is somewhat dangerous (hence the
        // `unsafe`!). This is creating a raw view into our module's
        // `WebAssembly.Memory` buffer, but if we allocate more pages for ourself
        // (aka do a memory allocation in Rust) it'll cause the buffer to change,
        // causing the `Float32Array` to be invalid.
        //
        // As a result, after `Float32Array::view` we have to be very careful not to
        // do any memory allocations before it's dropped.
        unsafe {
            let vert_array = js_sys::Float32Array::view(&vertices);

            context.buffer_data_with_array_buffer_view(
                GL::ARRAY_BUFFER,
                &vert_array,
                GL::STATIC_DRAW,
            );
        };
        context.bind_buffer(GL::ARRAY_BUFFER, None);

        let len = vertices.len() as i32;

        Ok(Buffer {
            len, buffer
        })
    }

    fn draw(&self, context: &GL) {
        context.bind_buffer(GL::ARRAY_BUFFER, Some(&self.buffer));
        context.vertex_attrib_pointer_with_i32(0, 2, GL::FLOAT, false, 0, 0);
        context.enable_vertex_attrib_array(0);

        context.draw_arrays(
            GL::TRIANGLES,
            0,
            self.len / 2,
        );

        context.bind_buffer(GL::ARRAY_BUFFER, None);
    }
}


struct Texture {
    tex: WebGlTexture,
}

impl Texture {
    fn new(context: &GL, img: &HtmlImageElement) -> Result<Texture, JsValue> {
        let tex = context.create_texture()
            .ok_or_else(|| String::from("Unable to create texture object"))?;

        context.bind_texture(GL::TEXTURE_2D, Some(&tex));
        let format = GL::RGBA;
        let ptype = GL::UNSIGNED_BYTE;
        context.tex_image_2d_with_u32_and_u32_and_image(
            GL::TEXTURE_2D, 0, format as i32, format, ptype, img
        )?;

        context.tex_parameteri(GL::TEXTURE_2D, GL::TEXTURE_WRAP_S, GL::CLAMP_TO_EDGE as i32);
        context.tex_parameteri(GL::TEXTURE_2D, GL::TEXTURE_WRAP_T, GL::CLAMP_TO_EDGE as i32);
        context.tex_parameteri(GL::TEXTURE_2D, GL::TEXTURE_MIN_FILTER, GL::NEAREST as i32);
        context.tex_parameteri(GL::TEXTURE_2D, GL::TEXTURE_MAG_FILTER, GL::NEAREST as i32);
        context.bind_texture(GL::TEXTURE_2D, None);


        Ok(Texture { tex })
    }
}

struct TextureManager {
    textures: Rc<RefCell<HashMap<&'static str, Option<Texture>>>>
}

impl TextureManager {
    fn new() -> TextureManager {
        TextureManager { textures: Rc::new(RefCell::new(HashMap::new())) }
    }

    fn bind(&self, context: &GL, path: &'static str) {
        let mut textures = self.textures.borrow_mut();
        let tex = textures.entry(path).or_insert_with(|| { self.load_texture(path); None });
        context.active_texture(GL::TEXTURE0);
        context.bind_texture(GL::TEXTURE_2D, tex.as_ref().map(|t| { &t.tex }));
    }

    fn load_texture(&self, path: &'static str) {
        let weak = Rc::downgrade(&self.textures);

        wasm_bindgen_futures::spawn_local(async move {
            Self::load_texture_inner(weak, path).await;
        });
    }

    async fn load_texture_inner(textures: Weak<RefCell<HashMap<&'static str, Option<Texture>>>>, path: &'static str) {
        let document = web_sys::window().unwrap().document().unwrap();
        let canvas = document.get_element_by_id("canvas").unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas.dyn_into::<web_sys::HtmlCanvasElement>().unwrap();
        let context = canvas
            .get_context("webgl").unwrap()
            .unwrap()
            .dyn_into::<GL>().unwrap();


        let img = load_image(path).await;

        if let Some(textures) = textures.upgrade() {
            img.ok().and_then(|i| { Texture::new(&context, &i).ok() }).map(|t| {
                textures.borrow_mut().insert(path, Some(t));
            });
        }
    }
}


pub struct Sprite {
    pub x: f32,
    pub y: f32,
    pub w: f32,
    pub h: f32,
    pub img: &'static str
}

struct ProgramInfo {
    projection: web_sys::WebGlUniformLocation,
    offset: web_sys::WebGlUniformLocation,
    scale: web_sys::WebGlUniformLocation,
    sampler: web_sys::WebGlUniformLocation,
}

impl ProgramInfo {
    fn get_uniform_location(context: &GL, program: &WebGlProgram, name: &str)
        -> Result<web_sys::WebGlUniformLocation, String>
    {
        context.get_uniform_location(&program, name)
            .ok_or_else(|| { format!("Unable to find uniform {}", name)})
    }
    fn new(context: &GL, program: &WebGlProgram) -> Result<ProgramInfo, String> {
        let projection = Self::get_uniform_location(&context, &program, "uProjection")?;
        let offset = Self::get_uniform_location(&context, &program, "uOffset")?;
        let scale = Self::get_uniform_location(&context, &program, "uScale")?;
        let sampler = Self::get_uniform_location(&context, &program, "uSampler")?;

        Ok(ProgramInfo {
            projection,
            offset,
            scale,
            sampler,
        })
    }
}

pub struct Renderer {
    context: GL,
    buffer: Option<Buffer>,
    program_info: Option<ProgramInfo>,
    texture_manager: TextureManager,
}

impl Renderer {
    pub fn new() -> Result<Renderer, JsValue> {
        let document = web_sys::window().unwrap().document().unwrap();
        let canvas = document.get_element_by_id("canvas").unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas.dyn_into::<web_sys::HtmlCanvasElement>()?;

        let context = canvas
            .get_context("webgl")?
            .unwrap()
            .dyn_into::<GL>()?;

        let texture_manager = TextureManager::new();
        Ok(Renderer{
            context,
            buffer: None,
            program_info: None,
            texture_manager
        })
    }
    pub fn init(&mut self) -> Result<(), JsValue> {

        let context = &self.context;

        // context.clear_color(0.2,0.5,0.5,1.0);
        // context.clear(GL::COLOR_BUFFER_BIT);

        context.enable(GL::BLEND);
        context.blend_func(GL::ONE, GL::ONE_MINUS_SRC_ALPHA);
        context.pixel_storei(GL::UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
        context.pixel_storei(GL::UNPACK_FLIP_Y_WEBGL, 1);

        let vert_shader = compile_shader(
            &context,
            GL::VERTEX_SHADER,
            r#"
            uniform mat4 uProjection;
            uniform vec2 uOffset;
            uniform vec2 uScale;

            attribute vec2 position;
            varying highp vec2 vTextureCoord;

            void main() {
                gl_Position = uProjection * vec4(position * uScale + uOffset, 0.0, 1.0);
                vTextureCoord = position.xy;
            }
        "#,
        )?;
        let frag_shader = compile_shader(
            &context,
            GL::FRAGMENT_SHADER,
            r#"
            uniform sampler2D uSampler;
            varying highp vec2 vTextureCoord;

            void main() {
                gl_FragColor = texture2D(uSampler, vTextureCoord);
            }
        "#,
        )?;
        let program = link_program(&context, &vert_shader, &frag_shader)?;
        context.use_program(Some(&program));
        self.program_info = Some(ProgramInfo::new(&context, &program)?);

        self.buffer = Some(Buffer::new(&context, &[0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0])?);

        Ok(())
    }

    fn draw_sprite(&self, sprite: &Sprite) {
        let context = &self.context;
        let program_info = self.program_info.as_ref().unwrap();
        context.uniform2f(Some(&program_info.offset), sprite.x, sprite.y);
        context.uniform2f(Some(&program_info.scale), sprite.w, sprite.h);
        context.uniform1i(Some(&program_info.sampler), 0);
        self.texture_manager.bind(context, sprite.img);

        self.buffer.as_ref().unwrap().draw(context);
    }

    pub fn draw(&self, scene: &[Sprite]) {
        let context = &self.context;

        context.clear_color(0.0, 0.0, 0.0, 1.0);
        context.clear(GL::COLOR_BUFFER_BIT);

        let program_info = self.program_info.as_ref().unwrap();
        context.uniform_matrix4fv_with_f32_array(Some(&program_info.projection), false,
            &[8.0/800.0, 0.0, 0.0, 0.0,
            0.0, 8.0/800.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0]
        );

        for s in scene {
            self.draw_sprite(s);
        }
    }
}
