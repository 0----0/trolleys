use std::collections::HashMap;

pub struct InputHandler {
    keys: HashMap<u32, bool>
}

impl InputHandler {
    pub fn new() -> InputHandler {
        InputHandler {
            keys: HashMap::new()
        }
    }

    pub fn add_key(&mut self, keycode: u32) {
        self.keys.entry(keycode).or_insert(false);
    }

    pub fn set_key(&mut self, keycode: u32, down: bool) {
        self.keys.entry(keycode).and_modify(|d| { *d = down });
    }

    pub fn get_key(&self, keycode: u32) -> bool {
        *self.keys.get(&keycode).unwrap_or(&false)
    }
}
