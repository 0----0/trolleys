import("../pkg/index.js").catch(console.error).then(module => {

    let Game = module.Game;
    let asdf = Game.new();
    asdf.init();
    asdf.draw();

    document.addEventListener('keydown', (e) => { asdf.key_down(e.keyCode); });
    document.addEventListener('keyup', (e) => { asdf.key_up(e.keyCode); });

    function updateLoop() {
      asdf.update();
      window.setTimeout(updateLoop, 1000 / 60);
    }

    updateLoop();

    function animationLoop() {
      requestAnimationFrame( animationLoop );
      asdf.draw();
    }

    animationLoop();


    window.loadImage = function(src) {
        let img = new Image();
        img.src = src;
        return new Promise((resolve, reject) => {
            img.onload = () => { resolve(img); }
        });
    }

});
